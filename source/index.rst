Three Men in a Boat (To Say Nothing of the Dog)
===============================================

:by: JEROME K. JEROME
     author of
     “idle thoughts of an idle fellow,”
     “stage land,” etc.
:Illustrations by: A. Frederics.

.. image:: img/p0b.jpg
   :alt: Decorative graphic
   :align: center
   :width: 15%

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   chapter1
   chapter2
   chapter3

Preface
=======

The chief beauty of this book lies not so much in its literary style, or in the extent and usefulness of the information it conveys, as in its simple truthfulness.  Its pages form the record of events that really happened.  All that has been done is to colour them; and, for this, no extra charge has been made.  George and Harris and Montmorency are not poetic ideals, but things of flesh and blood—especially George, who weighs about twelve stone.  Other works may excel this in depth of thought and knowledge of human nature: other books may rival it in originality and size; but, for hopeless and incurable veracity, nothing yet discovered can surpass it.  This, more than all its other charms, will, it is felt, make the volume precious in the eye of the earnest reader; and will lend additional weight to the lesson that the story teaches.

London, August, 1889.

.. image:: img/p1b.jpg
   :alt: Graphic of three men in a rowing boat"
   :align: center
   :width: 50%
